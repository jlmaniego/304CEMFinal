'use strict'

var storage = require('node-persist')

storage.initSync()

// can write 2 tests
exports.checkData = function(data) {
	if (data.id === undefined) return false
	return true
}

// 2 tests
exports.addData = function(data) {
	if (storage.getItemSync(data.id) !== undefined) {
		return false
	}
	storage.setItemSync(data.id, data)
	return true
}

// 2 tests
exports.updateData = function(data) {
	if (storage.getItemSync(data.id) === undefined) {
		return false
	}
	storage.setItemSync(data.id, data)
	return true
}

// 2 tests
exports.countItems = function() {
	const favourites = storage.values()
	if (favourites.length) return true
	return false
}

/*'use strict'

const request = require('request')

search('javascript', (err, data) => {
	console.log('START')
	if (err) {
		console.log(err.message)
	} else {
		console.log(data)
	}
	console.log('END')
})

function search(query, callback) {
	searchByString(query).then( data => {
		console.log('search query complete')
		callback(null, data)
	}).catch( err => {
		console.log('ERROR')
		console.log(err)
		callback(err)
  })
}

function searchByString(query) {
	return new Promise( (resolve, reject) => {
		const url = `https://api.edamam.com/search?q=${query}&app_id=e569755f&app_key=5f5964a141b570051f51ffffc2ec7892`

  	request.get(url, (err, res, body) => {
    	if (err) {
        	reject(Error('failed to make API call'))
    	}
    	const data = JSON.parse(body)

    	resolve(data)
  	})
	})
}*/