'use strict'

var storage = require('node-persist') //requires node-persist

storage.initSync()

exports.checkData = function(data) { //check data ufunction
	if (data.label === undefined) return false //if data.label is not defined, return false
	return true
}

exports.addData = function(data) {  //add data function
	if (storage.getItemSync(data) !== undefined) {
		return false
	}
	const file = data.file
	storage.setItemSync('Favourites', data.label)
	console.log("Added") //returns added to console log 
	console.log(storage.getItem('favourite'))
	return true
}

exports.updateData = function(data) {
	console.log(data)
	if (storage.getItemSync('Favourites') !== undefined) {
		const fav = data.label
		const oldFile = storage.getItemSync('Favourites')
		const combine = [oldFile, fav] //puts together exisiting file in storage and adds new fav
		storage.setItemSync('Favourites', combine) //stores 'Favourites' name and combination of files
		return true
	}
	return false
}

exports.countItems = function() {
	const favourites = storage.values()
	if (favourites.length) return true 
	return false
}

exports.getData = function() { //getdata function
	if(storage.getItemSync('Favourites') !== undefined){ //gets data, if it's not undefined return true
		return true
	} 
	return false //else return false
}

exports.removeItem = function(data){ //removing item
	const file = data.file
	if(storage.removeItemSync('Favourites')) return true //removes Item from favourites
	return false
	
}
