//index.js acts as server
const restify = require('restify') //uses restify
const server = restify.createServer() //creates server

const recipes = require('./recipes') //gets information from recipes.js
const favourites = require('./favourites') //gets information from favourites.js

server.use(restify.fullResponse())
server.use(restify.queryParser())
server.use(restify.bodyParser())
server.use(restify.authorizationParser())

server.get('/recipes', recipes.recipeSearch) //calls the function recipeSearch inside recipe.js
server.get('/favourites', favourites.get) //calls get function inside favourites.js
server.post('/favourites', favourites.validate, favourites.add) //calls validate and add function inside favourites.js
server.put('/favourites', favourites.update) //calls update function inside favourites.js
server.del('/favourites', favourites.delete)  //calls delete function inside favourites.js

const port = process.env.PORT || 8000 //runs on port 8000
server.listen(port, err => console.log(err || `App running on port ${port}`)) //console returns a message to let you know it is running on port 8000
