'use strict'

const storage = require('node-persist')  // use basic file-based data persistence
const testableCode = require('./testableCode')
storage.initSync()
const recipes = []

exports.validate = function validate (req, res, next) { //validate function in favourites //request, response and goes to the next function when done
  const fav = req.body
  if (!fav) res.send(400, {message: 'Need to send data'}) //if user doesn't enter data, this returns this message
  if (!fav.label) res.send (400, {message: 'label: REQUIRED'}) //if user enters data but not the appropriate one this message is returned
		
		next ()
	
}

exports.list = function list (req, res) {
  dbConnect(req, res, userstore => res.send({favourites: userstore.values()}))
}

exports.add = function add (req, res) {
	if(testableCode.addData(req.body)){ //connected to testableCode.js for testing //adds data
		res.send(201, {message: 'Favourite has been added'})
		res.end
	}else{
		res.send(400, {message: 'Already in favourites', data: req.body})
		res.end
	}

	
		}

exports.get = function (req, res) {
	if(testableCode.getData){
		const data = storage.getItemSync('Favourites')  //gets Favourite file
		res.send(data)
	}else{
		res.send(400, {message: "No Favourites File"}) //returns this message if favourites file doesn't exist
	}
}

exports.update = function update(req, res) {
	const data = req.body
	console.log(data)
	if (testableCode.updateData(data)) {
		res.send(201, "Favourites updated")
		res.end()
	} else {
		res.send(400, 'Favourite does not exist')
		res.end()
	}
}

exports.delete = function delete_ (req, res, next) {
	const data = req.body
	if(testableCode.removeItem(data)){
		storage.removeItemSync('Favourites') //'Favourites' is the file name you're creating
		res.send({message: "Item Deleted"}) //if deleted, returns this message
	}else{
		//fail silently
	}
}


function dbConnect (req, res, callback) {

  // define the storage file for this user
  const username = req.authorization.basic.username
  const userstore = storage.create({dir: `./.node-persist/favourites/${username}`})
  
  // initialise the connection (creates a new file if necessary)
  userstore.init(err => {

    if (err) {
      console.log(err)
      return res.send(500, {message: 'Unable to initialise data store'})
    } else {
      return callback(userstore)  // can now read and write to the storage file
    }

  })
	
}