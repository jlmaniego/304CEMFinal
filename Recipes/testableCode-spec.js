'use strict'

const testableCode = require('./testableCode') //requires testableCode.js for testing
const storage = require('node-persist')

describe('testableCode', () => {

	beforeEach( () => {
		storage.initSync()
		storage.clearSync() //wipes out storage before testing
		const data = {label: 'cake'}
		storage.setItemSync('Favourites',data)
		console.log(storage.getItemSync(data.label))
  })
	
	
	it('should add a valid item', () => {

		  const data = {label: 'chocolate'} //adds chocolate
			const added = testableCode.addData(data) //calls addData function in testableCode.js
			expect(added).toBe(true) //expected to be true
			const item = storage.getItemSync('Favourites') //stores Favourites file name
			expect(item).toEqual('chocolate') //data is expected to be equal to chocolate
			expect(storage.length()).toBe(1)
	})
			 
	it('should not add a duplicate item', () => {
		  const data = {label: 'chocolate'} //shouldn't add chocolate as it has already been added
		  storage.setItemSync('Favourites',data)	
			const added = testableCode.addData(data)
			expect(added).toBe(true) //expected to be true
			expect(storage.length()).toBe(1) //only asking for one since doesn't allow duplicate
	})
	
	it('should update the list', () => {
		  const data = {label: 'Pizza'} //data would be 'Pizza', should be added to the list
		  storage.setItemSync('Favourites',data)	
			const added = testableCode.updateData(data) //calles updateData function in testableCode.js
			expect(added).toBe(true)
			expect(storage.length()).toBe(1)
	})
	
				 
})