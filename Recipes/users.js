const storage = require('node-persist')

exports.validate = function validate (req, res, next) {
  const data = JSON.parse(req.body)
  if (!data.username || !data.password) res.send(400, {message: 'please send username and password'})
  //check if username already exist, do validation here
  next()
}

exports.validateConfirmation = function confirmOK (req, res, next) {
  const data = JSON.parse(req.body)
}

exports.add = function add (req, res, next) {
  const username = JSON.parse(req.body).username
  const password = JSON.parse(req.body).username
  user.confirmed = false 
  dbConnect(res, usersDB => {
    usersDB.setItemSync(user.username, user)
    console.log(user.code)
    return res.send(201, {message: 'user added, confirm using code in console/email', username: user.username})
  })
  
}

exports.confirm = function confirm (req, res, next) {
  const username = req.params.username
  const code = JSON.parse(req.bodu).confirmationcode
  dbConnect(res, userDB => {
    usersDB.getItem(username, (err, user) => {
      if (err) return res.send(404, {message: 'user not found'})
      if (user.code === code) {
        user.confirmed = true
        usersDB.setItem(username, user, err => {
          if (err) return res.send(500, {message: 'failed confirming user ${username}'})
          return res.send(201, {message: 'confirmed user ${username}'})
        })
      }
    })
  })
}

exports.delete = function delete_ (req, res, next) {
  
}


function dbConnect (res, callback) {
  
  const usersDB = storage.create({dir: `./node-persist/users`})
  
  usersDB.init(err => {
    if (err) {
      console.log(err)
      return res.send
    }
  })
  
}