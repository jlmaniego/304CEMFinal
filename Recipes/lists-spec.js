'use strict'

const lists = require('./lists')
const schema = require('./schema')

describe('favourites list', () => {
	beforeEach( done => {
		schema.List.remove({}, err => {
			if (err) expect(true).toBe(false)
			new schema.List({name: 'colours', list: ['milk', 'wheat', 'egg']}).save( (err, list) => {
				if (err) expect(true).toBe(false)
				schema.List.count({}, (err, count) => {
					if (err) expect(true).toBe(false)
					expect(count).toBe(1)
					done()
				})
			})
		})
	})
	describe('add', () => {
		it('should add a valid list', done => {
			const favourites = {
				name: 'favourites', 
				list: ['bread', 'butter', 'cheese']
			}

			lists.add(favourites, (err, data) => {
				if (err) expect(true).toBe(false)
				schema.List.count({}, (err, count) => {
					if (err) expect(true).toBe(false)
					expect(count).toBe(2)
					done()
				})
			})
		})
	})
	describe('count', () => {
		it('should find one list', done => {
			lists.count( (err, count) => {
				if (err) expect(true).toBe(false)
				expect(count).toBe(1)
				done()
			}) 
		})
	})
	describe('remove', () => {
		it('should remove an existing list', done => {
			lists.remove('ingredients').then( () => {
				schema.List.count({}, (err, count) => {
					if (err) expect(true).toBe(false)
					expect(count).toBe(0)
					done()
				})
			}).catch( err => {
				if (err) expect(true).toBe(false)
				done()
			})
		})
	})
})