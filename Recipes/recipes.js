'use strict'

const request = require('request') //uses request
const storage = require('node-persist') //uses node-persist for storage
storage.initSync()


exports.recipeSearch = function(req, res, next){
	const query = req.params.q
	const url = `https://api.edamam.com/search?q=${query}&app_id=e569755f&app_key=5f5964a141b570051f51ffffc2ec7892` //gets information from third party API
	console.log(url) //returns url on console
	
	request.get(url, function(err, resp, body){
		const json = JSON.parse(body)
		if(!err && resp.statusCode == 200) {
			console.log(json.hits[0].recipe.label)
			const recipes = [] 
			for( let i = 0; i< json.hits.length; i++) { //loops to show multiple recipes
				console.log(json.hits[i].recipe.label)
				let recipe = {
					name: json.hits[i].recipe.label, //name of the recipe is called 'label' which is under recipe which is under the array hits
					ingredients: json.hits[i].recipe.ingredientLines, //ingredients list is in ingredientLines
					link: json.hits[i].recipe.url //link to the recipe so users can read the instructions
				}
				recipes.push(recipe) //this adds to the end of recipe
				next ()
			}
			
			res.send({recipes: recipes}) //this sends information required to postman such as name, ingredients and link
			//storage.setItemSync('Archive', recipes);		
		}
	})
	
}

